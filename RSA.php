<?php

/**
 * RSA相关函数
 * openssl_pkey_get_public() 从证书中提取公钥
 * openssl_pkey_get_private() 从证书中提取私钥
 * openssl_public_encrypt() 公钥加密
 * openssl_private_decrypt() 私钥解密
 * openssl_private_encrypt() 私钥加密
 * openssl_public_decrypt() 公钥解密
 * base64_encode() 使用base64对数据重新编码
 * base64_decode() 将base64的数据解码
 */

#第一步：将密匙定义为常量
define('RSA_PUBLIC', '-----BEGIN PUBLIC KEY-----
MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCmkANmC849IOntYQQdSgLvMMGm
8V/u838ATHaoZwvweoYyd+/7Wx+bx5bdktJb46YbqS1vz3VRdXsyJIWhpNcmtKhY
inwcl83aLtzJeKsznppqMyAIseaKIeAm6tT8uttNkr2zOymL/PbMpByTQeEFlyy1
poLBwrol0F4USc+owwIDAQAB
-----END PUBLIC KEY-----');
define('RSA_PRIVATE', '-----BEGIN PRIVATE KEY-----
MIICdgIBADANBgkqhkiG9w0BAQEFAASCAmAwggJcAgEAAoGBAKaQA2YLzj0g6e1h
BB1KAu8wwabxX+7zfwBMdqhnC/B6hjJ37/tbH5vHlt2S0lvjphupLW/PdVF1ezIk
haGk1ya0qFiKfByXzdou3Ml4qzOemmozIAix5ooh4Cbq1Py6202SvbM7KYv89syk
HJNB4QWXLLWmgsHCuiXQXhRJz6jDAgMBAAECgYAIF5cSriAm+CJlVgFNKvtZg5Tk
93UhttLEwPJC3D7IQCuk6A7Qt2yhtOCvgyKVNEotrdp3RCz++CY0GXIkmE2bj7i0
fv5vT3kWvO9nImGhTBH6QlFDxc9+p3ukwsonnCshkSV9gmH5NB/yFoH1m8tck2Gm
BXDj+bBGUoKGWtQ7gQJBANR/jd5ZKf6unLsgpFUS/kNBgUa+EhVg2tfr9OMioWDv
MSqzG/sARQ2AbO00ytpkbAKxxKkObPYsn47MWsf5970CQQDIqRiGmCY5QDAaejW4
HbOcsSovoxTqu1scGc3Qd6GYvLHujKDoubZdXCVOYQUMEnCD5j7kdNxPbVzdzXll
9+p/AkEAu/34iXwCbgEWQWp4V5dNAD0kXGxs3SLpmNpztLn/YR1bNvZry5wKew5h
z1zEFX+AGsYgQJu1g/goVJGvwnj/VQJAOe6f9xPsTTEb8jkAU2S323BG1rQFsPNg
jY9hnWM8k2U/FbkiJ66eWPvmhWd7Vo3oUBxkYf7fMEtJuXu+JdNarwJAAwJK0YmO
LxP4U+gTrj7y/j/feArDqBukSngcDFnAKu1hsc68FJ/vT5iOC6S7YpRJkp8egj5o
pCcWaTO3GgC5Kg==
-----END PRIVATE KEY-----');

#第二步：进行公钥加密
$public_key = openssl_pkey_get_public(RSA_PUBLIC);
if(!$public_key){
    die('公钥不可用');
}

//第一个参数是待加密的数据只能是string，第二个参数是加密后的数据,第三个参数是openssl_pkey_get_public返回的资源类型,第四个参数是填充方式
$return_en = openssl_public_encrypt('123456', $crypted, $public_key);
if(!$return_en){
    return('加密失败，请检查RAS秘钥');
}
$eb64_cry = base64_encode($crypted);
echo '公钥加密数据：'.$eb64_cry;
echo '<hr/>';

#测试使用私钥进行解密
$private_key = openssl_pkey_get_private(RSA_PRIVATE);
if(!$private_key){
    die('私钥不可用');
}
$return_de = openssl_private_decrypt(base64_decode($eb64_cry), $decrypted, $private_key);
VAR_DUMP($return_de);
if(!$return_de){
    return('解密失败，请检查RAS秘钥');
}
echo "私钥解密数据:".$decrypted;
echo "<hr>";



#私钥加密，公钥解密的方式：
//私钥加密
$private_key = openssl_pkey_get_private(RSA_PRIVATE);
if(!$private_key){
    die('私钥不可用');
}
$return_en = openssl_private_encrypt("hello world222222", $crypted, $private_key);
if(!$return_en){
    return('加密失败,请检查RSA秘钥');
}
$eb64_cry = base64_encode($crypted);
echo "私钥加密数据".$eb64_cry;
echo "<hr>";

//公钥解密
$public_key = openssl_pkey_get_public(RSA_PUBLIC);
if(!$public_key){
    die('公钥不可用');
}
$return_de = openssl_public_decrypt(base64_decode($eb64_cry), $decrypted, $public_key);
if(!$return_de){
    return('解密失败,请检查RSA秘钥');
}
echo "公钥解密数据:".$decrypted;
echo "<hr>";

?>

<!--JS使用RSA加密解密-->
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>使用jsencrypt执行OpenSSL的RSA加密，解密</title>
</head>
<!--引入jsencrypt.js-->
<script src="https://cdn.bootcss.com/jsencrypt/3.0.0-beta.1/jsencrypt.js"></script>
<script type="text/javascript">
    //公钥
    var PUBLIC_KEY = 'MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCmkANmC849IOntYQQdSgLvMMGm\n8V/u838ATHaoZwvweoYyd+/7Wx+bx5bdktJb46YbqS1vz3VRdXsyJIWhpNcmtKhY\ninwcl83aLtzJeKsznppqMyAIseaKIeAm6tT8uttNkr2zOymL/PbMpByTQeEFlyy1\npoLBwrol0F4USc+owwIDAQAB';
    //私钥
    var PRIVATE_KEY = 'MIICdgIBADANBgkqhkiG9w0BAQEFAASCAmAwggJcAgEAAoGBAKaQA2YLzj0g6e1h\nBB1KAu8wwabxX+7zfwBMdqhnC/B6hjJ37/tbH5vHlt2S0lvjphupLW/PdVF1ezIk\nhaGk1ya0qFiKfByXzdou3Ml4qzOemmozIAix5ooh4Cbq1Py6202SvbM7KYv89syk\nHJNB4QWXLLWmgsHCuiXQXhRJz6jDAgMBAAECgYAIF5cSriAm+CJlVgFNKvtZg5Tk\n93UhttLEwPJC3D7IQCuk6A7Qt2yhtOCvgyKVNEotrdp3RCz++CY0GXIkmE2bj7i0\nfv5vT3kWvO9nImGhTBH6QlFDxc9+p3ukwsonnCshkSV9gmH5NB/yFoH1m8tck2Gm\nBXDj+bBGUoKGWtQ7gQJBANR/jd5ZKf6unLsgpFUS/kNBgUa+EhVg2tfr9OMioWDv\nMSqzG/sARQ2AbO00ytpkbAKxxKkObPYsn47MWsf5970CQQDIqRiGmCY5QDAaejW4\nHbOcsSovoxTqu1scGc3Qd6GYvLHujKDoubZdXCVOYQUMEnCD5j7kdNxPbVzdzXll\n9+p/AkEAu/34iXwCbgEWQWp4V5dNAD0kXGxs3SLpmNpztLn/YR1bNvZry5wKew5h\nz1zEFX+AGsYgQJu1g/goVJGvwnj/VQJAOe6f9xPsTTEb8jkAU2S323BG1rQFsPNg\njY9hnWM8k2U/FbkiJ66eWPvmhWd7Vo3oUBxkYf7fMEtJuXu+JdNarwJAAwJK0YmO\nLxP4U+gTrj7y/j/feArDqBukSngcDFnAKu1hsc68FJ/vT5iOC6S7YpRJkp8egj5o\npCcWaTO3GgC5Kg==';
    //使用公钥加密
    var encrypt = new JSEncrypt();
    //encrypt.setPrivateKey('-----BEGIN RSA PRIVATE KEY-----'+PRIVATE_KEY+'-----END RSA PRIVATE KEY-----');
    encrypt.setPublicKey('-----BEGIN PUBLIC KEY-----' + PUBLIC_KEY + '-----END PUBLIC KEY-----');
    var encrypted = encrypt.encrypt('张三13866889910');
    console.log('加密后数据:%o', encrypted);
    //使用私钥解密
    var decrypt = new JSEncrypt();
    //decrypt.setPublicKey('-----BEGIN PUBLIC KEY-----' + PUBLIC_KEY + '-----END PUBLIC KEY-----');
    decrypt.setPrivateKey('-----BEGIN RSA PRIVATE KEY-----'+PRIVATE_KEY+'-----END RSA PRIVATE KEY-----');
    var uncrypted = decrypt.decrypt(encrypted);
    console.log('解密后数据:%o', uncrypted);
</script>

</html>

<?php
$post = "Kg/MVfMqcTIm59m4Egv3SHtDrTSnTQ2nOR3DjOn6T77tnSOP3EqxGIfUAexflSiN5mKwXHxfPbR6A3xVVaR67SfEr9pfbvbDc9zmmh8q9axwomQH0CANoECBEOYoMkVbXsCdgdRLJ/vaMPUj8tKNJf86cYlkYV/CMEWfuaIGaHw=";

$return_de = openssl_private_decrypt(base64_decode($post), $decrypted, $private_key);
if(!$return_de){
    return('解密失败，请检查RAS秘钥');
}
echo "JS加密数据解密:".$decrypted;
echo "<hr>";
?>




